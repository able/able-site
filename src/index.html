<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>ABLE - A Basic Lisp Editor</title>
  <link rel="stylesheet" type="text/css" href="style.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
</head>

<body>
 <div class="header">
   <h1>ABLE - A Basic Lisp Editor</h1>
 </div>
<p>
  ABLE is an open source Common Lisp editor for Mac, Linux and Windows.
  <!--<a href="files/using-able.mov">screencast</a>-->
  ABLE's features include a listener with command history,
  syntax colouring, symbol completion, jump to definition, parenthesis matching,
  configurable indentation, Hyperspec lookup and call-tips.
  ABLE is distributed under the
  <a href="http://www.opensource.org/licenses/mit-license.php">MIT</a> license.
</p>
<p>ABLE was formerly hosted at another site (<a href="http://web.archive.org/web/*/http://phil.nullable.eu">Internet Archive</a>); with Phil's permission, development has moved to <a href="http://common-lisp.net">common-lisp.net</a>.</p>


<h2>Mailing lists</h2>
<ul>
  <li><a href="http://common-lisp.net/cgi-bin/mailman/listinfo/able-devel">ABLE-devel</a> -- seek help, suggest changes, request binary releases, etc.</li>
  <li><a href="http://common-lisp.net/cgi-bin/mailman/listinfo/able-announce">ABLE-announce</a> -- receive messages whenever a (source or binary) release is made.</li>
  <li><a href="http://common-lisp.net/cgi-bin/mailman/listinfo/able-cvs">ABLE-cvs</a> -- receive messages whenever the source files change.</li>
</ul>


<h2>Installation</h2>
<p>
  Version 0.20 was compatible with
  <a href="http://ccl.clozure.com/">CCL</a>,
  <a href="http://www.sbcl.org/">SBCL</a> and
  <a href="http://clisp.cons.org/">CLISP</a>. It depends on
  <a href="http://www.tcl.tk/">Tcl/Tk</a>,
  <a href="http://www.peter-herth.de/ltk/">LTK</a>,
  <a href="http://www.weitz.de/cl-fad/">CL-FAD</a> and
  <a href="http://www.cliki.net/trivial-gray-streams">TRIVIAL-GRAY-STREAMS</a>.
  ABLE can be loaded and started with:
</p>
<pre>
  (asdf:oos 'asdf:load-op 'able)  ; load ABLE
  (able::start)                   ; start ABLE
</pre>
<p>The latest source release is <a href="files/able-0.21.zip">able-0.21.zip</a>.  Development sources are available from <a href="http://common-lisp.net/gitweb?p=projects/able/able.git">Gitweb</a>.  Binary releases will be made available as time permits and requests are made.</p>

<!--
Where are these? Replace them with Fare's XCVB scripts?
<p>
  A selection of operating system and compiler specific startup scripts can be found in the
  'scripts' directory. These allow you to start ABLE more like a conventional program (i.e.
  without needing to start your Lisp and load from the REPL).
</p>
-->


<h2>Getting started</h2>
<p>
  ABLE comprises a single window split into two frames. The top frame is the editor
  and the bottom the listener (the interface to the REPL). Information messages and
  call-tips are displayed in the message bar along the bottom. All commands are invoked
  with keyboard shortcuts which are
  documented below. Commands which require further input, such as filenames, prompt
  for it in the listener.
</p>
<p>
  Throughout this document, as each feature of ABLE is introduced, relevant key bindings or
  associated configuration settings will be described. These take the form of code snippets
  which can be supplied in a configuration file. The examples given show the default values.
  To override the default values, create a file named .able in your home directory and provide
  any settings you wish to change. If you're unsure of where your home directory is, evaluate
  the following in the listener:
</p>
<pre>
  (user-homedir-pathname)
</pre>
<p>
  Note that it's not necessary to have this file for ABLE to run nor is it necessary to
  override every setting if you do, just those you wish to change.
</p>


<h2>Working with files</h2>
<p>
  The standard file system commands are available through the following shortcuts:
</p>
<pre>
  (setf able::*key-new-file* "&lt;Control-n&gt;")
  (setf able::*key-open-file* "&lt;Control-o&gt;")
  (setf able::*key-close-file* "&lt;Control-w&gt;")
  (setf able::*key-save-file* "&lt;Control-s&gt;")
  (setf able::*key-save-as-file* "&lt;Control-S&gt;")
</pre>
<p>
  Prompts for filenames appear in the listener which supports pathname completion by
  pressing the TAB key. In the event of multiple possible completions, alternatives will be
  shown in the message bar. Completion is only provided for .lisp files and directories.
</p>
<p>
  In addition to opening a file, ABLE can also load a file into both the editor and the Lisp
  system, reload it and compile it:
</p>
<pre>
  (setf able::*key-load-file* "&lt;Control-l&gt;")
  (setf able::*key-reload-file* "&lt;Control-r&gt;")
  (setf able::*key-compile-file* "&lt;Control-k&gt;")
</pre>
<p>
  ABLE can open multiple files at once and switch between them with
</p>
<pre>
  (setf able::*key-next-file* "&lt;Control-b&gt;")
</pre>
<p>
  Finally, when you want to quit ABLE you can press
</p>
<pre>
  (setf able::*key-quit-able* "&lt;Control-q&gt;")
</pre>
<p>
  If there are any unsaved files you will be prompted to take action.
</p>


<h2>Working with code</h2>
<p>
  ABLE can be used to edit any text file although it can't be considered a general purpose
  text editor, instead focussing on Common Lisp specific features. The standard editing
  commands are:
</p>
<pre>
  (setf able::*key-cut* "&lt;Control-x&gt;")
  (setf able::*key-copy* "&lt;Control-c&gt;")
  (setf able::*key-paste* "&lt;Control-v&gt;")
  (setf able::*key-select-all* "&lt;Control-a&gt;")
</pre>
<p>
  As you type code, ABLE will highlight known symbols. This includes both symbols from the
  <a href="http://www.lispworks.com/documentation/HyperSpec/Front/index.htm">Hyperspec</a> as
  well as those from your own code. When a file is loaded or saved, it's indexed for all
  defining forms. In addition, you can watch a set of directories using
</p>
<pre>
  (setf able::*watch-directories* nil)
</pre>
<p>
  By providing a list of directory names here, all symbols found will be indexed. Because the
  index is updated whenever a file is saved, you can comfortably work on your own systems in
  these directories and know the index is up to date.	As well as providing syntax colouring,
  this setting is used in symbol completion and jump to definition. Symbol completion allows
  you to type the start of a symbol name and then press
</p>
<pre>
  (setf able::*key-code-complete* "&lt;Tab&gt;")
</pre>
<p>
  to cycle through all possible completions of that prefix in turn. Jump to definition, invoked
  with
</p>
<pre>
  (setf able::*key-lookup* "&lt;Control-d&gt;")
</pre>
<p>
  will locate the definition of the symbol under the cursor. If the definition is in another file
  then it will be opened (or switched to if already open). If lookup is pressed on a
  symbol defined by Common Lisp itself then the relevant section of the Hyperspec is loaded.
  The location of the Hyperspec can be configured with
</p>
<pre>
  (setf able::*hyperspec-root*
    "http://www.lispworks.com/documentation/HyperSpec/Body/")
</pre>
<p>
  By default, ABLE uses the version on the <a href="http://www.lispworks.com/">LispWorks</a>
  server but this can be replaced with a local URL. The browser used to open the Hyperspec
  can be specified in
</p>
<pre>
  (setf able::*web-browser*
    #+:able-windows "C:/Progra~1/Intern~1/iexplore.exe"
    #+:able-linux "/usr/bin/firefox"
    #+:able-macosx "open")
</pre>
<p>
  As you write code, ABLE will show call-tips for known symbols in the message bar. This
  includes the name of the function and its lambda list. Note that the call-tips are mined from
  the Lisp system itself and not all symbols are covered in all implementations.
</p>
<p>
  ABLE indents code as you type based on idiomatic Lisp coding standards. Because these are not
  enforced, either by the compiler or the community, it's possible to extend the rules to suit
  your preferences using
</p>
<pre>
  (setf able::*indentation-rules*
    '(("if" . 4)
      ("cond" . 6)
      ("and" . 5)
      ("or" . 4)
      ("eq" . 4)
      ("loop" . 6)))
</pre>
<p>
  The default indentation rule specifies a 2 space indent for each open SEXP and a 1 space
  indent for an open double parenthesis such as a let block's binding list. Anything provided
  in the above setting overrides the default on a per symbol basis. Configuration comprises a
  list of cells in which each entry is the name of the symbol and the number of spaces to
  indent it. To reformat a block of code, place the cursor inside it and press
</p>
<pre>
  (setf able::*key-reformat* "&lt;Control-j&gt;")
</pre>
<p>
  To copy code to the listener, place the cursor at the closing parenthesis of an SEXP and press
</p>
<pre>
  (setf able::*key-copy-to-repl* "&lt;Control-e&gt;")
</pre>
<p>
  Once at the listener, pressing ENTER will evaluate the form. The listener has a history buffer
  which can be navigated with the up and down arrow keys. To clear the current command in the
  listener press ESCAPE. Repeatedly pressing ESCAPE will toggle the cursor between the editor
  and the listener.
</p>
<p>
  Standard find and goto line facilities are available with
</p>
<pre>
  (setf able::*key-find* "&lt;Control-f&gt;")
  (setf able::*key-find-again* "&lt;Control-g&gt;")
  (setf able::*key-goto-line* "&lt;Control-i&gt;")
</pre>
<p>
  These prompt for input at the listener and can be cancelled with ESCAPE.
</p>
<p>
  Placing the cursor on a closing parenthesis and pressing
</p>
<pre>
  (setf able::*key-macro-expand* "&lt;Control-m&gt;")
</pre>
<p>
  will macroexpand the code, outputting the results in the listener.
</p>


<h2>Other Settings</h2>
<p>
  Although ABLE has a deliberately spartan, keyboard focussed UI, a few settings can be
  configured. Various colours can be overridden using either HEX values or colour names
</p>
<pre>
  (setf able::*highlight-text* "#2D1E27")
  (setf able::*highlight-background* "#FFFFFF")
  (setf able::*highlight-primary* "#1900D5")
  (setf able::*highlight-secondary* "#991C1C")
  (setf able::*highlight-comments* "#00732A")
  (setf able::*highlight-paren-match* "#F3752F")
  (setf able::*highlight-error* "#FF4343")
</pre>
<p>
  The font used throughout ABLE can be set (Courier is a safe default which Tk guarantees to be
  present on all operating systems but I recommend Monaco on OS X, Consolas on Windows and
  Monospace on Ubuntu).
</p>
<pre>
  (setf able::*buffer-font* "Courier 12 normal roman")
</pre>
<p>
  The initial window placement and size can be specified to suit your display:
</p>
<pre>
  (setf able::*window-width* 640)
  (setf able::*window-height* 480)
  (setf able::*window-x* 30)
  (setf able::*window-y* 30)
</pre>
<p>
  Finally, the number of lines to display in the listener can be set:
</p>
<pre>
  (setf able::*listener-lines* 10)
</pre>


<h2>FAQ</h2>
<p>
  As with many FAQs, this one's a mixture of real questions that often come up and
  feeble justifications for why things are implemented the way they are!
</p>
<p>
  <strong>Why do you no longer supply the binaries?</strong>
  <br/>
  Unfortunately, whatever I supplied, people wanted something different! Be it
  a newer version of CLISP, an older version of CLISP, SBCL on Windows or 32-bit CCL on OSX.
  Unfortunately I just don't have the time to make everyone happy so now I provide only the
  source code. As ABLE is just a Common Lisp library, anyone who's managed to set
  up a Lisp compiler and ASDF can be up and running quite quickly. 
</p>
<p>
  <strong>Which Lisp compilers are supported?</strong>
  <br/>
  ABLE is compatible with <a href="http://ccl.clozure.com/">CCL</a>,
  <a href="http://www.sbcl.org/">SBCL</a> and
  <a href="http://clisp.cons.org/">CLISP</a>.
</p>
<p>
  <strong>Why do I need to install Tcl/Tk?</strong>
  <br/>
  ABLE uses LTK which is a Common Lisp interface to the Tk GUI. Other GUI toolkits do
  exist for CL but LTK is the only one which meets my requirements: open source, cross compiler,
  cross platform and builds without errors. If you own a Mac then you already have Tcl/Tk
  installed. If you're on Windows or Linux then you can get Tcl/Tk from
  <a href="http://www.tcl.tk/">here</a> or
  <a href="http://www.activestate.com/activetcl/">here</a> or
  <a href="http://www.equi4.com/tclkit/">here</a>.
</p>
<p>
  <strong>Why doesn't ABLE load on my Windows box?</strong>
  <br/>
  Ensure you're running a supported Lisp compiler and that ASDF can find ABLE and its
  dependencies. First start up your lisp and try to load ABLE (but don't start it). If
  everything loads then the problem is with Tcl/Tk. Ensure that Tcl/Tk's wish.exe program
  is visible on your system path by running it from the command line. If it is but the GUI
  window doesn't load, the problem is most likely a clash with some resident Windows programs,
  especially those loaded in the system tray area. Try experimenting to see if closing any helps.
</p>
<p>
  <strong>Why is there no menu?</strong>
  <br/>
  ABLE provides a purely keyboard driven user interface. For example when opening a file,
  a text prompt is provided in the listener so it wouldn't make sense to lift your hand
  off the keyboard, pick up the mouse to select a menu option and then return
  your hand to the keyboard to enter the pathname.
</p>
<p>
  <strong>When I select paste, why does the program stop responding?</strong>
  <br/>
  Using Paste when the Tk clipboard is empty causes LTK or Tcl/Tk to silently hang.
  Unfortunately this seems to happen before ABLE can intercept the event.
  A bug report has been filed with the LTK developers although I'm not sure if
  there's anything they can do about it.
</p>
<p>
  <strong>Why is there no debugger?</strong>
  <br/>
  A debugger is the most important of the missing features in ABLE. Adding one is likely to
  be a big task but is something I plan to work on in the future.
</p>
<p>
  <strong>Why does the syntax colouring think that strings contain code?</strong>
  <br/>
  This is a limitation of the current algorithm which favors speed over accuracy, especially
  in the case of multi-line constructs which are basically ignored.
</p>


<h2>Thanks</h2>
<p>
  Thanks to Phil Armitage, Myroslav Vus, Michael Ben-Yosef, Sean Ross, Jussi Salmela, and Masayuki Onjo.
</p>

 <div class="check">
   <a href="http://validator.w3.org/check/referer">Valid XHTML 1.0 Strict</a>
 </div>
</body>
</html>
